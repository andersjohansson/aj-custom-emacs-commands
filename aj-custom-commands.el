;;; aj-custom-commands.el --- A collection of useful commands  -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2022  Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Keywords: convenience
;; Created: 2019-10-16
;; Modified: 2022-10-04
;; Package-Requires: ((emacs "25.1") (org "9.0") (xah-replace-pairs "2.0") (request "0.3.0") (pcsv "1.3.7"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a mixed collection of commands that I don’t use all the
;; time (so I keep them out of init.el) but which are handy now and then.

;;; Code:


;;; Ta bort buffer och fil
;; från http://whattheemacsd.com/file-defuns.el-02.html
;;;###autoload
(defun aj/delete-current-buffer-file ()
  "Removes file connected to current buffer and kills buffer."
  (interactive)
  (let ((filename (buffer-file-name))
        (buffer (current-buffer)))
    (if (not (and filename (file-exists-p filename)))
        (ido-kill-buffer)
      (when (y-or-n-p "Are you sure you want to remove this file? ")
        (delete-file filename)
        (kill-buffer buffer)
        (message "File '%s' successfully removed" filename)))))

;;; beginning-or-indentation
;;Från http://www.emacswiki.org/emacs/misc-cmds.el
;;;###autoload
(defun aj/beginning-or-indentation ()
  "Move cursor to beginning of this line or to its indentation.
If at indentation position of this line, move to beginning of line.
If at beginning of line, move to beginning of previous line.
Else, move to indentation position of this line.

With arg N, move backward to the beginning of the Nth previous line.
Interactively, N is the prefix arg."
  (interactive)
  (cond ((bolp) (back-to-indentation))
        ((save-excursion (skip-chars-backward " \t") (bolp)) ; At indentation.
         (forward-line 0))
        (t (back-to-indentation))))

;;; Edit regions etc.

;; from: https://www.mail-archive.com/gnu-emacs-sources@gnu.org/msg00034.html
;;;###autoload
(defun aj/randomize-region (beg end)
  "Randomize strings in region"
  (interactive "r")
  (if (> beg end)
      (let (mid) (setq mid end end beg beg mid)))
  (save-excursion
    ;; put beg at the start of a line and end and the end of one --
    ;; the largest possible region which fits this criteria
    (goto-char beg)
    (or (bolp) (forward-line 1))
    (setq beg (point))
    (goto-char end)
    ;; the test for bolp is for those times when end is on an empty
    ;; line; it is probably not the case that the line should be
    ;; included in the reversal; it isn't difficult to add it
    ;; afterward.
    (or (and (eolp) (not (bolp)))
        (progn (forward-line -1) (end-of-line)))
    (setq end (point-marker))
    (let ((strs (aj/shuffle-list
                 (split-string (buffer-substring-no-properties beg end)
                               "\n"))))
      (delete-region beg end)
      (dolist (str strs)
        (insert (concat str "\n"))))))

(defun aj/shuffle-list (list)
  "Randomly permute the elements of LIST.
All permutations equally likely."
  (let ((i 0)
        j
        temp
        (len (length list)))
    (while (< i len)
      (setq j (+ i (random (- len i))))
      (setq temp (nth i list))
      (setcar (nthcdr i list) (nth j list))
      (setcar (nthcdr j list) temp)
      (setq i (1+ i))))
  list)


;;; laegg senast modifierad till filnamn
(require 'dired)

;;;###autoload
(defun aj/last-mod-rename (files)
  "Add mtime as Y-m-d in filenames of one or many FILES."
  (interactive "FFil: ")
  (unless (listp files) (setq files (list files)))
  (dolist (file files)
	(when (file-writable-p file)
	  (let ((mtime (format-time-string "%Y-%m-%d-" (nth 5 (file-attributes file)))))
		(rename-file file (concat (file-name-directory file) mtime (file-name-nondirectory file)))))))


;; laegg till den till dired actions.
;;;###autoload
(defun aj/dired-do-last-mod-rename ()
  "Add mtime as Y-m-d in filenames of marked files."
  (interactive)
  (let ((files (dired-get-marked-files)))
	(when (y-or-n-p "Add mtime to filenames? ")
	  (aj/last-mod-rename files))))


;;; aj/org-commit-template
(require 'org-inlinetask)

;;;###autoload
(defun aj/org-commit-template (&optional prepend dir level)
  "Create git commit template from org-mode changes.
Antar att DIR är ett git-rep. Om LEVEL ges så bara headlines av
den leveln, annars alla nya. PREPEND läggs längst fram i filen."
  (with-temp-buffer
	(when dir (cd dir))
	(message "defdir %s" default-directory)
	(aj/write-string-to-file
	 (let ((tmp (or prepend ""))
		   (hregx (format
                   "^+\\*\\{%d,%d\\}\\(?: +\\)?\\(?: +\\(?:\\[#.\\]\\)\\)?\\(?: +\\(.*?\\)\\)??\\(?:[ 	]+\\(:[[:alnum:]_@#%%:]+:\\)\\)?[ 	]*$"
                   (or level 1)
                   (or level (1- org-inlinetask-min-level)))))
	   (insert (shell-command-to-string "git diff --cached"))
	   (goto-char (point-min))
	   (while (re-search-forward hregx nil t)
		 (setq tmp (concat tmp (match-string-no-properties 1) "\n")))
	   tmp)
	 ".git/info/commit-template.txt")))


;;; Rensa upp tex-kataloger
;;;###autoload
(defun aj/clean-tex-dirs (dirs)
  "Clean up .tex-aux directories"
  (interactive (list (list (read-directory-name "Directory: "))))
  (save-window-excursion
    (let (to-relink to-delete buf)
      (dolist (dir dirs)
        (when (file-directory-p dir)
          (let ((linkfiles (directory-files dir t "pdf\\(pc\\)?$")))
            (dolist (f linkfiles)
              (let ((ft (file-truename f)))
                (when (and (not (string= f ft)) (string-match "\\.tex-aux" ft))
                  (push (cons f ft) to-relink)))))
          (when (file-directory-p (expand-file-name ".tex-aux" dir))
            (push (expand-file-name ".tex-aux" dir) to-delete))
          (when (file-directory-p (expand-file-name ".auctex-auto" dir))
            (push (expand-file-name ".auctex-auto" dir) to-delete))))

      (when (or to-relink to-delete)
        (setq buf (switch-to-buffer (generate-new-buffer "*clean-tex-dirs*")))
        (insert "---- Länka om ----\n")
        (dolist (tr to-relink)
          (insert (car tr) " ⮕ " (cdr tr) "\n"))
        (insert "\n---- Ta bort ----\n")
        (dolist (d to-delete)
          (insert d "\n"))
        (if (y-or-n-p "Länka om och ta bort? ")
            (progn
              (dolist (tr to-relink)
                (delete-file (car tr))
                (when (file-exists-p (cdr tr))
                  (copy-file (cdr tr) (car tr) t t)))
              (dolist (d to-delete)
                (delete-directory d t t))))
        (kill-buffer buf)))))

;;;###autoload
(defun aj/clean-tex-dirs-recursive (root)
  (interactive "DRoot directory: ")
  (let ((root (or root "~/")))
    (aj/clean-tex-dirs
     (cl-delete-duplicates
      (split-string
       (shell-command-to-string
        (format "find %s -name '*.tex' -printf '%%h\n'" root))
       "\n") :test 'string=))))

;;;###autoload
(defun aj/annotations-from-laesplattan ()
  (interactive)
  (require 'pcsv)
  (let* ((dbfile "/run/media/aj/READER/Sony_Reader/database/books.db")
         (dbfile (if (file-readable-p dbfile)
                     dbfile
                   (read-file-name "Database file (books.db)" nil nil t)))
         (callstring (format
                      "sqlite3 -csv \"%s\" \"%%s\""
                      (url-encode-url (concat "file://" dbfile "?mode=ro"))))
         books books2 bookid annotations)
    (with-temp-buffer
      (call-process-shell-command
       (format callstring "select _id, title, file_name from books")
       nil t)
      (setq books (pcsv-parse-buffer)))
    (unless books
      (user-error "No books?"))
    (setq books2 (cl-loop for (id name filename) in books
                          collect
                          (cons (concat name "	"
                                        (propertize filename 'face 'shadow))
                                id)))
    (setq bookid
          (cdr-safe
           (assoc (completing-read "Choose book: " books2) books2))
          ;; (helm-comp-read "Choose book: "
          ;;                 (cl-loop for (id name filename) in books
          ;;                          collect
          ;;                          (cons (concat name "	"
          ;;                                        (propertize filename 'face 'shadow))
          ;;                                id))
          ;;                 :must-match t :fuzzy t)
          )
    (with-temp-buffer
      (call-process-shell-command
       (format callstring
               (format
                "select page, markup_type, name, marked_text from annotation where content_id = %s order by page"
                bookid))
       nil t)
      (setq annotations (pcsv-parse-buffer)))
    (if annotations
        (progn (switch-to-buffer (generate-new-buffer "*PRS annotations*"))
               (cl-loop for (page type note marked) in annotations
                        do
                        (when (equal "11" type) ; if the note has text
                          (insert note ":\n"))
                        (insert "#+begin_quote\n"
                                marked
                                "\n"
                                "(p. "
                                (if (string-match "^\\([0-9]+\\)\\.0$" page)
                                    (match-string 1 page)
                                  page)
                                ")\n"
                                "#+end_quote\n\n"))
               (goto-char (point-min))
               (org-mode))
      (message "No annotations found for that book"))))

;;; Texdoc loading and retrieval. (don’t have complete texdoc installed)
(require 'consult)
(require 'request)

(defvar aj/texdoc-history nil)

(defvar aj/texdoc-arg nil)

(defconst aj/texdoc-dir "~/texdoc")

;;;###autoload
(defun aj/texdoc (arg)
  (interactive "P")
  (let* ((completion-regexp-list '("/\\.$" "/\\.\\.$"))
         (aj/texdoc-arg arg)
         (cands (aj/texdoc-cands))
         (cand (completing-read "Texdoc: " cands nil nil nil aj/texdoc-history)))
    (if (member cand cands)
        (aj/texdoc-open-or-download-newer cand)
      (aj/texdoc-search-and-download cand))))

(defun aj/texdoc--c-to-fn (cand)
  (expand-file-name (concat cand ".pdf") aj/texdoc-dir))

(defun aj/texdoc-open-or-download-newer (doc)
  (let ((fn (aj/texdoc--c-to-fn doc)))
    (if (and (not (equal '(16) aj/texdoc-arg))
             (or (equal '(4) aj/texdoc-arg)
                 (< 200 (time-to-number-of-days
                         (time-subtract (current-time)
                                        (file-attribute-modification-time
                                         (file-attributes fn)))))))
        (aj/texdoc-search-and-download doc t)
      (consult-file-externally fn))))

(defun aj/texdoc-search-and-download (doc &optional maybe-open-old)
  (let ((fn (aj/texdoc--c-to-fn doc))
        (request-backend 'url-retrieve))
    (request
      (format "https://texdoc.org/pkg/%s" doc)
      :timeout 10
      :parser 'buffer-string
      :error (cl-function (lambda (&key _data response &allow-other-keys)
                            (message "Failed contacting texdoc.net, response status: %s"
                                     (request-response-status-code response))
                            (when maybe-open-old
                              (consult-file-externally fn))))
      :success (cl-function
                (lambda (&key data response &allow-other-keys)
                  (if (string= "application/pdf"
                               (request-response-header response "content-type"))
                      (progn
                        (with-temp-buffer
                          (setq buffer-file-coding-system 'raw-text)
                          (insert data)
                          (write-file fn))
                        (consult-file-externally fn))
                    (user-error "Not found")))))))

(defun aj/texdoc-cands ()
  (when (file-accessible-directory-p aj/texdoc-dir)
    (mapcar #'file-name-base (directory-files aj/texdoc-dir nil "^[^.]"))))

;;; Whitespace cleanup

(defvar whitespace-trailing-regexp)
;;;###autoload
(defun aj/delete-trailing-and-double-space-org ()
  (interactive)
  (when (eq major-mode 'org-mode)
    (require 'whitespace)
    (let ((savedpoint (point)))
      (save-excursion
        (save-restriction
          (widen)
          (goto-char (point-min))
          (while (search-forward-regexp whitespace-trailing-regexp nil t)
            (goto-char (match-end 1))
            (unless (eq savedpoint (point))
              (delete-region (match-beginning 1) (match-end 1))))
          (goto-char (point-min))
          (while (search-forward-regexp "[[:blank:]]\\{2,\\}" nil t)
            (unless (or (org-in-src-block-p) (org-at-table-p)
                        (eq (match-beginning 0) (point-at-bol)))
              (replace-match " "))))))))


;;; org mode-stuff

;;;###autoload
(defun aj/org-table-to-tree ()
  "Transforms an org table to a (sub)tree.
Each row an entry, spaces between columns"
  (interactive)
  (if (org-at-table-p)
      (let ((level (1+ (or (org-current-level) 0)))
            (table (remove 'hline (org-table-to-lisp)))
            (beg (org-table-begin))
            (end (org-table-end)))
        (delete-region beg end)
        (dolist (row table)
          (insert
           (make-string level (string-to-char "*"))
           " "
           (mapconcat 'identity row " ")
           "\n")))
    (message "Not at a table")))


;;; Fix org files
(require 'xah-replace-pairs)
(require 'org-table)

;;;###autoload
(defun aj/anonymize-from-table ()
  "Read in names from org-table at point and replace with random strings.
Reads in first column. Also runs aj/org-replace-links-by-descs to
avoid file links leaking info."
  (interactive)
  (when (org-at-table-p)
    (random t)
    (let* ((col (org-table-current-column))
           (table (org-table-to-lisp))
           (table (if (eq (nth 1 table) 'hline)
                      (cl-subseq table 1)
                    table))
           (table (cl-remove 'hline table))
           (namelist
            (cl-loop for el in table
                     collect
                     (list (regexp-quote (nth (1- col) el))
                           (regexp-quote
                            (substring
                             (secure-hash 'md5 (number-to-string (random))) 0 12))))) )

      (xah-replace-pairs-region (point-min) (point-max) namelist t)
      (aj/org-replace-links-by-descs "file"))))

(require 'org)

;;;###autoload
(defun aj/org-replace-links-by-descs (&optional type)
  "Replace org links by descriptions.
With TYPE only replace links of that type."
  (interactive)
  (save-excursion
    (save-restriction
      (when (use-region-p)
        (narrow-to-region (region-beginning) (region-end)))
      (goto-char (point-min))
      (while (search-forward-regexp org-link-bracket-re nil t)
        (when (or (and type
                       (equal type
                              (car-safe
                               (save-match-data
                                 (split-string (match-string-no-properties 1) ":")))))
                  (not type))
          (let ((remove (list (match-beginning 0) (match-end 0)))
                (description (if (match-end 2)
                                 (match-string-no-properties 2)
                               (match-string-no-properties 1))))
            (apply 'delete-region remove)
            (insert description)))))))

;;;###autoload
(defun aj/org-remove-all-properties ()
  "Delete all property blocks in subtree"
  (interactive)
  (org-map-tree
   (lambda () (if-let ((be (org-get-property-block (point))))
             (delete-region (car be) (cdr be))))))

;;;###autoload
(defun aj/org-id-todo-state (id state)
  "Switch todo state of org entry with id ID to STATE."
  (if-let ((m (org-id-find id 'marker)))
      (with-current-buffer (marker-buffer m)
        (save-excursion
          (goto-char m)
          (set-marker m nil)
          (org-todo state)))
    (error "Cannot find entry with ID \"%s\"" id)))

;;; See recent messages
;;;###autoload
(defun aj/message-peek ()
  (interactive)
  (pop-to-buffer "*Messages*" 'display-buffer-pop-up-window)
  (goto-char (point-max)))

;;; Run meld on selected files
;;;###autoload
(defun aj/meld-compare (file1 file2)
  (interactive "fFil 1: \nfFil 2: ")
  (start-process "meld" "meld" "meld" file1 file2))


;;; Straight rebuild this package
(require 'straight)

;;;###autoload
(defun aj/straight-rebuild-this-package (&optional recursive)
  "Rebuild package in current dir. Prefix argument for recursive"
  (interactive "P")
  (if-let ((m (string-match (concat "^"
                                    (regexp-opt (list
                                                 (straight--repos-dir)
                                                 (straight--build-dir)))
                                    "\\(.+\\)$")
                            default-directory))
           (path (car (f-split (match-string 1 default-directory))))
           ;; reverse lookup from path to package names 🙂:
           (packages (cl-loop for package being the hash-keys of straight--recipe-cache
                              using (hash-values recipe)
                              if (string= path (plist-get recipe :local-repo))
                              collect package)))
      (cl-loop for p in packages do
               (straight-rebuild-package p recursive))
    (call-interactively #'straight-rebuild-package)))

;;;###autoload
(defun aj/straight-pull-multiple-packages (packages &optional from-upstream)
  "Try to pull packages PACKAGES from the primary remote.
PACKAGES is a list of strings naming packages. Interactively, select
PACKAGES from the known packages in the current Emacs session
using `completing-read-multiple'. With prefix argument FROM-UPSTREAM, pull
not just from primary remote but also from upstream (for forked
packages)."
  (interactive (list (aj/straight--select-multiple-packages "Pull packages"
                                                            #'straight--installed-p)
                     current-prefix-arg))
  (dolist (package packages)
    (straight-pull-package package from-upstream)))

(defun aj/straight-pull-multiple-packages-and-deps (packages &optional from-upstream)
  "Try to pull PACKAGES and their (transitive) dependencies.
PACKAGES, their dependencies, their dependencies, etc. are pulled
from their primary remotes.

PACKAGES is a list of strings naming packages. Interactively, select
PACKAGES from the known packages in the current Emacs session
using `completing-read-multiple'. With prefix argument FROM-UPSTREAM,
pull not just from primary remote but also from upstream (for
forked packages)."
  (interactive (list (aj/straight--select-multiple-packages "Pull packages"
                                                            #'straight--installed-p)
                     current-prefix-arg))
  (dolist (package packages)
    (straight-pull-package-and-deps package from-upstream)))

(defun aj/straight--select-multiple-packages (message &optional filter)
  "Use `completing-read-multiple' to select packages.
MESSAGE is displayed as the prompt; it should not end in punctuation
or whitespace.

FILTER is a function accepting one argument: a straight style recipe plist.
If it returns nil, the package is not considered a selection candidate."
  (completing-read-multiple
   (concat message ": ")
   (let ((packages nil))
     (maphash (lambda (package recipe)
                (when (or (null filter)
                          (funcall filter (plist-put recipe :package package)))
                  (push package packages)))
              straight--recipe-cache)
     (nreverse packages))
   (lambda (_) t)
   'require-match))

;;; edit variable

(require 'cl-extra)
(require 'help-fns)
;;;###autoload
(defun aj/edit-variable-value (variable)
  "Edit the value of VARIABLE in a ‘lisp-interaction-mode’ buffer."
  (interactive ;; from ‘describe-variable’
   (let ((v (variable-at-point))
	     (enable-recursive-minibuffers t)
         (orig-buffer (current-buffer))
	     val)
     (setq val (completing-read
                (format-prompt "Describe variable" (and (symbolp v) v))
                #'help--symbol-completion-table
                (lambda (vv)
                  ;; In case the variable only exists in the buffer
                  ;; the command we switch back to that buffer before
                  ;; we examine the variable.
                  (with-current-buffer orig-buffer
                    (or (get vv 'variable-documentation)
                        (and (boundp vv) (not (keywordp vv))))))
                t nil nil
                (if (symbolp v) (symbol-name v))))
     (list (if (equal val "")
	           v (intern val)))))

  (pop-to-buffer "*var-edit*" 'display-buffer-pop-up-window)
  (lisp-interaction-mode)
  (insert "(setq " (symbol-name variable) "\n'")
  (cl-prettyprint (symbol-value variable))
  (insert "\n)"))

;;; replace from csv table
(require 'pcsv)
(require 'xah-replace-pairs)
(defun aj/replace-from-csv-table (csvfile separator)
  "Replace pairs from CSVFILE using SEPARATOR in current buffer."
  (interactive (list (read-file-name "Csv file: " nil nil t)
                     (read-char "Separator character: ")))
  (let* ((pcsv-separator separator)
         (table (with-temp-buffer
                  (insert-file-contents csvfile)
                  (pcsv-parse-buffer))))

    (xah-replace-pairs-region
     (point-min) (point-max) table t t)))


;;; Zotero db
(defun aj/zotero-check-unlinked ()
  "Report unlinked files in zotero library folder."
  (interactive)
  (when-let ((db (sqlite-open "~/.zotero-profile/zotero.sqlite"))
             (att (sqlite-select db "select path from itemAttachments where linkMode = 2 and contentType = 'application/pdf'"))
             (att (mapcar #'car att))
             (files (directory-files "~/bibliotek/" t "\\.pdf$"))

             (diff (cl-nset-difference files att :test #'equal))
             (ds (sort diff #'string-lessp)))
    (sqlite-close db)
    (embark-export-dired ds)
    (auto-revert-mode -1)))

(defun aj/zotero-get-unrenamed ()
  "Report unrenamed files in zotero db."
  (interactive)
  (when-let ((db (sqlite-open "~/.zotero-profile/zotero.sqlite"))
             (it (sqlite-select db
                                "SELECT items.key, items.libraryID
FROM items JOIN itemAttachments USING (itemid)
where items.libraryID = 1 and itemAttachments.linkMode = 0 AND itemAttachments.contentType = 'application/pdf'")))
    (pop-to-buffer (generate-new-buffer "*unrenamed*"))
    (cl-loop for (key libid) in it do
             (insert
              (org-zotxt-make-item-link `( :key ,(concat (number-to-string libid) "_" key)
                                           :citation "cit"))
              "\n"))
    (org-mode)))


;;; Diff pdfs
;;;###autoload
(defun aj/pdfdiff (pdf1 pdf2)
  "Diff the text of PDF1 and PDF2 with ediff."
  (interactive (list (read-file-name "File 1: " nil nil t nil)
                     (read-file-name "File 2: " nil nil t nil)))
  (cl-loop with bufs = nil
           with buf = nil
           for f in (list pdf1 pdf2)
           collect (setq buf (generate-new-buffer (concat "*" (file-name-nondirectory f) "-for-diff*"))) into bufs
           do (with-current-buffer buf
                (call-process-shell-command (format "pdftotext \"%s\" /dev/stdout"
                                                    (expand-file-name f))
                                            nil t nil)
                (text-mode))
           finally do
           (apply #'ediff-buffers bufs)))


;;; Clean old elc-files in dir (mostly for emacs-build)

(defun aj/clean-old-elc (dir)
  "Recursively clean out old .elc files in DIR."
  (interactive "D")
  (cl-loop for f in (directory-files-recursively dir "\\.elc$")
           when (file-newer-than-file-p (substring f 0 -1) f)
           do ;; (message "Gammal: %s" f)
           (delete-file f)
           ))


(provide 'aj-custom-commands)
;;; aj-custom-commands.el ends here
